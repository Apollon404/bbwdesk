from django.urls import path
from .  import  views

app_name = "aduseradmin"
urlpatterns = [
    path('login/', views.loginPage, name='login'),
    path('register/', views.registerPage, name='register'),
   
    # universal views for logged in users
    path('logout/', views.logoutPage, name='logout'),
    path('account/', views.accountPage, name='account'),

    # Administration of Accounts (Staff_only)
    # path('admin/', views.adminPage, name='admin'),
    
    # groups
    path('groups/create', views.createGroup, name='group_create'),
    path('groups/<int:pk>/delete', views.deleteGroup, name='group_delete'),
    path('groups/<int:pk>/update', views.updateGroup, name='group_upate'),

    # users
    path('user/<int:pk>/view', views.viewUser, name='user_view'),
    path('user/<int:pk>/delete', views.deleteUser, name='user_delete'),
    path('user/<int:pk>/update', views.updateUser, name='user_update'),
    path('createuser/', views.createUser, name='user_create'),
]