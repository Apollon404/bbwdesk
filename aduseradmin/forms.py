from django.forms import ModelForm
from bbwdesk.models import UserDetail
from django.contrib.auth.models import User


class UserDetailForm(ModelForm):
    class Meta:
        model = UserDetail
        fields = ['initals','user']

