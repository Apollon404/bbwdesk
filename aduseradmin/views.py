from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse
from django.contrib.auth.forms import UserCreationForm, UserChangeForm

from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib import messages
from bbwdesk.decorators import ajax_required 

from bbwdesk.models import UserDetail
from .forms import UserDetailForm

import copy

"""
     Account Pages that dont require a login
"""

def loginPage(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(request, username=username, password=password)

        if user is not None:
            login(request, user)
            return redirect('bbwdesk:index')
        else:
            messages.info(request, 'Username OR password is incorrect')

    return render(request, 'aduseradmin/users_login_content.html')


def registerPage(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            user = form.cleaned_data.get('username')

            messages.success(request, 'Account was created for ' + user)

            return redirect('aduseradmin:login')
    else:
        form = UserCreationForm()

    context = {'form': form}
    return render(request, 'aduseradmin/users_register_content.html', context)


"""
    Accout Management Pages
"""
@login_required
def accountPage(request):
    return render(request, 'aduseradmin/account_management.html')

@login_required
def logoutPage(request):
    logout(request)
    return redirect('bbwdesk:index')


"""
    Group Managment Views ( Staff Only )
"""
@login_required
@ajax_required
def createGroup(request):
    return HttpResponse("Hi")

@login_required
@ajax_required
def deleteGroup(request, pk):
    return HttpResponse("Hi")

@login_required
@ajax_required
def updateGroup(request, pk):
    return HttpResponse("Hi")


"""
    User Management Views (Staff Only)
"""
@login_required
def viewUser(request, pk):
    user = User.objects.get(pk=pk)
    detail = UserDetail.objects.get(user=user.pk)

    context = {'user': user, 'detail' : detail}

    return render(request, 'aduseradmin/users_account_view.html', context)

@login_required
def deleteUser(request, pk):
    user = get_object_or_404(User, pk=pk)
        
    if request.method == 'POST': # Confirmed Delete
        UserDetail.objects.get(user=user.pk).delete()
        user.delete()
        return redirect('bbwdesk:index')
        
    return render(request, 'aduseradmin/users_account_delete.html', {'user' : user })

@login_required
def updateUser(request, pk):
    user = User.objects.get(pk=pk)
    userDetail = UserDetail.objects.get(user=user.id)
    

    if request.method == 'POST':
        # Filter Post for the models
        user.username = request.POST.get('username')
        user.first_name =  request.POST.get('first_name')
        user.last_name =  request.POST.get('last_name')

        userDetail.intials = request.POST.get('intials')
        
        user.save()
        userDetail.save()

        messages.success(request, 'Account-Data from' + user.username +' has been updated')

        return redirect('aduseradmin:user_update', pk)
    elif request.method == 'GET':
        userDetailForm = UserDetailForm(instance=userDetail)
        userForm = UserChangeForm(instance=user)
        
        context = {'user_f': userForm , 'detail_f' : userDetailForm}
        
        return render(request, 'aduseradmin/users_account_update.html', context)
    

@login_required
def createUser(request):
    if request.method == 'POST':
        u_form = UserCreationForm(
            { 'username' : request.POST.get('username'),
             'password1' : request.POST.get('password1'),
             'password2' : request.POST.get('password2')}
        )
    
        
        if u_form.is_valid():
            user = u_form.save()
            d_form = UserDetailForm({ 'initals' : request.POST.get('initals'),
                'user' : user.pk
            })
            
            if d_form.is_valid():  
                d_form.save()

                messages.success(request, 'Account was created for ' + user.username)
                return redirect('aduseradmin:user_view', user.pk)
            else: # d_form not valid
                user.delete()
                messages.error(request, 'User not created')  
        
    
    else: # method == 'GET'
        u_form = UserCreationForm()
        d_form = UserDetailForm()


    context = { 'user_f': u_form, 'detail_f': d_form }
    return render(request, 'aduseradmin/users_account_create.html', context)