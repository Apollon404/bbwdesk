from django.urls import path

from . import views
from . import restViews

app_name = 'bbwdesk'
urlpatterns = [
     # index
     path('', views.index, name='index'), #user dashboard
     path('adminboard/', views.adminPage, name='admin'),
     # task 
     path('taskcreate/', views.task_create, name='task_create'),
     path('<int:pk>/edit/', views.task_edit, name='task_edit'),
     path('<int:pk>/delete/', views.task_delete, name='task_delete'),
     path('<int:pk>/view/', views.TaskDetail.as_view(), 
          {'task_list.html':'bbwdesk/task_view_content.html'}, name='task_view'),
     path('<int:pk>/submit/', views.taskSubmitPage, name='task_submit'),
     # assignment Urls
     path('assignment/<int:pk>/', views.assignmentPage, name='assginment_view'),

     # API
     path('getGroupList/', views.getGroupList, name='ajax_grouplist'),
     path('objByAssignId/', views.getObjectsbyAssignment),
     path('assign/', restViews.AssignmentSet.as_view(), name='AssignmentSet'),
     path('assign/<int:pk>', restViews.AssignmentDetail.as_view(), name='AssignmentDetail'),
     path('assignNested/', restViews.AssignmentNestedSet.as_view(), name='NestedAssignmentSet'),
     path('userSet/', restViews.ExtendedUserSet.as_view(), name='UserSet'),
     path('assignObjSet/', restViews.AssignmentObjectSet.as_view(), name='AssignObjSet'),
]
