from django.apps import AppConfig


class BbwdeskConfig(AppConfig):
    name = 'bbwdesk'
