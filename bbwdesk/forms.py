
from .models import Task
import django.forms as forms

class TaskForm(forms.ModelForm):
    priority = forms.IntegerField(max_value=5, min_value=0)
    status = forms.IntegerField(max_value=3, min_value=0)
    progress = forms.IntegerField(max_value=10,min_value=0)
    deadline = forms.DateField()

    class Meta:
        model = Task
        fields = ('title', 'description', 'users', 'priority', 
                    'status', 'progress', 'deadline')
        widgets = {
            'status': forms.NumberInput(attrs={'value': 0})
        }
