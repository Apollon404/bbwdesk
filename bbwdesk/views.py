from django.shortcuts import get_object_or_404, render, redirect
from django.http import HttpResponse, JsonResponse
from django.views.generic.edit import DeleteView
from django.views.generic import DetailView
from django.urls import reverse_lazy
from django.contrib.auth.decorators import login_required
from django.core import serializers
from django.contrib.auth.models import Group, User


from .models import Task, Assignment, UserDetail, AssignmentObject
from .forms import TaskForm
from .decorators import ajax_required
from .utils import objectToJson

# Create your views here.

@login_required
def index(request):
    assignments = Assignment.objects.filter(actors__in=UserDetail.objects.filter(user=request.user))

    context = {
        'assignments': assignments
    }

    return render(request,'bbwdesk/index_content.html', context)

@login_required
def task_edit(request, pk):     
    if request.method == 'POST':
        try:
            task = Task.objects.get(pk=pk)
            t_form = TaskForm(instance=task, data=request.POST)
        except Task.DoesNotExist:
            raise Http404("Task does not exist")

        if t_form.is_valid():
            u = t_form.save()
            return redirect("bbwdesk:index")
    
    elif request.method == 'GET':
        try:
            task = Task.objects.get(pk=pk)
            t_form = TaskForm(instance=task)
        except Task.DoesNotExist:
            return HttpResponse(status=404)
    
        return render(request, 'bbwdesk/task_edit_content.html', {'form': t_form, 'task': task})

@login_required
def task_delete(request, pk):
    task = get_object_or_404(Task, pk=pk)
    
    if request.method == 'POST': # Confirmed Delete
            task.delete()
            return redirect('bbwdesk:index')
    
    return render(request, 'bbwdesk/task_delete_content.html', {'task' : task })

@login_required
def task_create(request):
    if request.method == "POST":
        t_form = TaskForm(request.POST)

        if t_form.is_valid():
            u = t_form.save()
            return redirect("bbwdesk:index")
    
    else:
        t_form = TaskForm()
        context = {'form': t_form}
        return render(request, 'bbwdesk/task_create_content.html', context)



class TaskDetail(DetailView):
    model = Task


    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        instance = super().get_object()

        context['users'] = instance.users.all()

        return context

@login_required
def taskSubmitPage(request, pk):
    return render(request, 'bbwdesk/submission_page_content.html')

@login_required
def assignmentPage(request):
    return HttpResponse("WIP: Assignment View")

"""
    All views for the adminPage
"""

# TODO: Write decorator, that limits access to users with role "Ausbilder"
@login_required
def adminPage(request):
    return render(request, 'bbwdesk/desk_admin_content.html')


# ajax views
@ajax_required
@login_required
def getGroupList(request):
    groups = Group.objects.all()
    json = { "groups" : list() }
    for group in groups:
        group_json = dict()
        group_json['pk'] = group.id
        group_json['name'] = group.name
        
        users = User.objects.filter(groups__id=group.id)
        group_json['users'] = list()
        
        for user in users:
            user_obj = dict()
            user_obj['pk'] = user.id
            user_obj['name'] = user.username
            
            group_json['users'].append(user_obj)

        json["groups"].append(group_json)

    return JsonResponse(json, safe=False)


@ajax_required
@login_required
def getObjectsbyAssignment(request):
    json = {"objects": list()}
    
    if request.method == "GET":
        objects = AssignmentObject.objects.filter(
                                    assignment_id=request.GET["assign_id"])

        for obj in objects:
            obj = Task.objects.get(object_id=obj.id)
            json["objects"].append(objectToJson(obj))

    return JsonResponse(json, safe=False)