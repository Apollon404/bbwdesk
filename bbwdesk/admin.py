from django.contrib import admin

from .models import UserDetail, Task, Comment, Submission, Assignment, AssignmentObject

admin.site.register(UserDetail)
admin.site.register(Task)
admin.site.register(Comment)
admin.site.register(Submission)
admin.site.register(Assignment)
admin.site.register(AssignmentObject)


