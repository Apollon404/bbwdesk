/*
    Das object das die detailView auf der adminPage abliden soll

    Author: Paul Jena
*/
'use strict';


// Detail Unsorted List View that loads several Assingment objects (simple View Object)
// The raw Data gets loaded by a diffrent class
class DetailView {
    constructor(rootJquery) {
        this.root = rootJquery;
        this.rows = {
            data: [],
            add: (jquery) => {
                this.rows.data = this.rows.data || [];
                let row = $("<li></li>").append(jquery).addClass("list-group-item");
                this.rows.data.push(row)
            },
            clean: () => {
                this.rows.data.length = 0;
            }
        }
        this.load();
    }

    load(){
        for(let i = 0; i < this.root.children().length; ++i)
        {
            this.rows.data[i] = this.root.children()[i];
        }
    }

    draw() {
        this.clean();
        for (let i = 0; i < this.rows.data.length; ++i){
           
            let element = this.rows.data[i];
            this.root.append(element);
        }
    }

    clean() {
        this.root.empty();
    }
}

jQuery.fn.ListDetailView = function() {
    this.dt = this.dt || new DetailView(this);
    return this.dt;
}