"use strict";

let task_url = (pk) =>
  "/" + pk + "/view/";

 

let getTaskList_url = "/objByAssignId/";

// Event-Listener
function assignment_click(eve)
{
  let assignment_element = eve.target;
  let assignment_id = assignment_element.getAttribute("assign_id");

  // active clicked element and deactivated old element
  $("#assignment_table .table-active").removeClass("table-active");
  assignment_element.classList.add("table-active");
  
  // ajax - request for new table - data
  $.ajax({
    url: getTaskList_url,
    type: "get",
    dataType:"json",
    data:
    {
      assign_id: assignment_id
    },
    success: (data) => {
        let objects = data.objects;
        let t = $("#obj-table").DataTable();
        t.rows().remove().draw();

        for(let i = 0; i < objects.length; ++i)
        {
          let obj = objects[i];
         
          let curr_row = t.row.add( [
            obj.title,
            obj.actors,
            obj.priority,
            obj.status,
            obj.deadline
          ]).node();
          
          $(curr_row).attr("pk", obj.pk)

        }
        t.draw()
        $("#obj-table > tbody > tr").dblclick(table_data_dbclick);
    }
    // TODO: Fehlerbehandlung
  });
}


// Event-Listener
function table_data_dbclick(eve)
{
    let data = eve.currentTarget;
    let data_id = data.getAttribute("pk");
    window.location.href = task_url(data_id);
}


// Even Listener Registration

$().ready(function () {
    $('#obj-table').DataTable({
      stateSave: true
    });
    $('.dataTables_length').addClass('bs-select');
  });

$().ready( function (){ 
    $("#assignment_table td").click(assignment_click);
  });

$().ready(() => {
  $("#obj-table > tbody > tr").dblclick(table_data_dbclick);
});