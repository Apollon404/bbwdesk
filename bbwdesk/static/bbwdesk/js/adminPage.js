'use strict';


/*
    Events
*/

function nestedToggle(event)
{
    if(event.target != event.currentTarget) return;
    let ele = event.currentTarget;
    ele.childNodes[1].classList.toggle("tree-active");
}


function table_data_dbclick(eve)
{
    let data = eve.currentTarget;
    let data_id = data.getAttribute("pk");
    window.location.href = urls.taskDetail(data_id);
}


function onclickEditButton(){
  //  if(event.target != event.currentTarget) return;
    let pk = $(event.target).parent().attr("pk");
    window.location.href = urls.userEdit(pk);
}


function onDblClickUser(){
    let user_pk = $(event.target).attr("pk");
    let caller = RestUtils.getAssignmentCaller(true, { actors: user_pk});
    let dt = $('#assign-detail').DataTable();
    dt.clear();

    caller.query().then( () => {
        for(let i = 0; i < caller.data.length; ++i){
            let a = caller.data[i];
    
            let row = dt.row.add( [
                a.name,
                a.editor,
                a.actors
            ]).node();
    
            $(row).attr("pk", a.id);
        }
        dt.draw();
    });
    
}

/*
    End Events
*/

function addEditButton(jquery){
    let button = $("<button></button>");
    button.addClass("desk-edit-btn");
    jquery.append(button);
}

// renders with ajax-request- data the user file explorer
function renderGroupList() {
    let render = (data) => { 
        let root = $('.tree-view');
        let groups = data.groups;
        for( let i = 0; i < groups.length; ++i)
        {
            let group = groups[i];
            let group_element = $("<li></li>").text(group.name);
            group_element.attr("pk", group.pk);
            group_element.addClass("tree-nested-head");

            let usr_list = $("<ul></ul>");
            usr_list.addClass("tree-nested");
        
            root.append(group_element);
          
            group_element.append(usr_list);
            let users = group.users;

            for(let ii = 0; ii < users.length; ++ii)
            {
                let user = users[ii];
                let usr_element = $("<li></li>").text(user.name);
                usr_element.attr("pk", user.pk)
                addEditButton(usr_element)
                usr_list.append(usr_element);
            }
        }
        
        root.on('click', '.tree-nested-head', nestedToggle);
        root.on('click', '.desk-edit-btn', onclickEditButton);
        root.on('dblclick', '.tree-nested > li', onDblClickUser);
    
        // TODO: append users elements

    }; // End of Rendering

    
    $.get({ // ajax Request
        url: urls.groupList,
        dataType: 'json'
    }).done(render);
}


/*
 ------------- Event Listeners for Admin Explorer -------
*/
$(document).ready(renderGroupList);
// toggling tree view

$(document).ready(() => {
    $("#createUser").click(() => {
        window.location.href = urls.createUser;
    });
});

$(document).ready(() => {
    $("#createGroup").click(() => {
        window.location.href = urls.createGroup;
    });
});

/*
--------------- Event Listeners for Detail View
*/
// Lists on startup all Assignments
$(document).ready(() => {
    let caller = new RestCaller('/assign/');
    let dt = $('#assign-detail').DataTable({
            paging: true,
       }
    );
    dt.rows().remove().draw();
   
    caller.query().then(() => {
        for(let i = 0; i < caller.data.length; ++i)
        {
            let assign = caller.data[i];

            let row = dt.row.add( [
                assign.name,
                assign.editor,
                assign.actors
            ]).node();

            $(row).attr("pk", assign.id);
        }
    
        dt.draw();
    });
});

