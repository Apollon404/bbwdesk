/*

    Author: Paul Jena
*/
'use strict';

let urls = {
    groupList: '/getGroupList/',
    createUser: '/createuser/',
    
    taskDetail: (pk) => {
        "/" + pk + "/view/";
    },
    taskEdit: (pk) => {
        return "/" + pk + "/edit/";
    },
    userEdit: (pk) => {
        return '/user/' + pk + '/update';
    },

    createGroup: '/creategroup/',

    // Task, Assignments etc.
    assignments: '/assign/',
    nestedAssign: '/assignNested/',
}    


class RestCaller {
    constructor(url){
        this.url = url;
        this.query_params = { };
        this.data;
        this.status;
    }

    query(method='GET'){
        let contextObj = this;

        return $.ajax(this.url, {
            data: this.query_params,
            dataType: "json",
            method: 'GET',
            success: (data, status, promise) =>{
                contextObj.data = data;
                contextObj.status = status;
            }
        });
    }
}

let RestUtils =  {
    
    getInitalsByPk: function (pk){
        let caller = new RestCaller('/userSet');
        caller.query_params = { id : pk };
        caller.query();

        return caller.data[0].detail.intials || null
    },

    getAssignmentCaller: function (getNested=true, query_params){
        let caller = new RestCaller(getNested ? urls.nestedAssign : urls.assignments);
        caller.query_params = query_params;

        return caller;
    }
}