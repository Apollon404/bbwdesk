from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator
from django.utils import timezone
from django.contrib.auth.models import User

# Create your models here.

class UserDetail(models.Model):
    user = models.OneToOneField(User, null=True, on_delete=models.CASCADE, related_name='detail')
    initals = models.CharField(max_length=4)

    def __str__(self):
        return self.initals


# All Assignment related models

class Assignment(models.Model):
    name = models.CharField(max_length=40, default="Assignment")
    editor = models.ForeignKey(User, on_delete=models.CASCADE, related_name="assign_editor")
    creation_date = models.DateField(default=timezone.now)
    actors = models.ManyToManyField(User, blank=True)

    def __str__(self):
        return self.name


# TODO: Needs more generic expression for searching a foreign key relation
class AssignmentObject(models.Model):
    assignment_id = models.ForeignKey(Assignment, on_delete=models.CASCADE, related_name="assignObjects")

    def __str__(self):
        try:
            task = Task.objects.get(object_id=self.id)
        except Task.DoesNotExist:
            return "Unassigned"

        return "Assignment-Object: " + task.title


# AssingmentObjects Subcategory Models

class Task(models.Model):
    object_id = models.OneToOneField(AssignmentObject, null=True,on_delete=models.CASCADE,related_name="taskObject")
    title = models.CharField(max_length=20)
    description = models.TextField(blank=True)
    users = models.ManyToManyField(UserDetail, blank=True)
    priority = models.IntegerField(default=0, 
                                validators=[
                                    MaxValueValidator(5),
                                    MinValueValidator(0),
                                ]
    )
    status = models.IntegerField(default=0, 
                                validators=[
                                    MaxValueValidator(3),
                                    MinValueValidator(0),
                                ]
    )
    progress = models.IntegerField(default=0,
                                    validators=[
                                    MaxValueValidator(10),
                                    MinValueValidator(0),
                                ]
    )
    deadline = models.DateField(default=timezone.now)


    def __str__(self):
        return self.title


# Comment and Submission System

class CommentThread(models.Model):
    id = models.AutoField(primary_key=True)
    object_id = models.OneToOneField(AssignmentObject, null=True,on_delete=models.CASCADE)

class Comment(models.Model):
    user_id = models.ForeignKey(UserDetail, blank=True, null=True, on_delete=models.CASCADE)
    text = models.TextField()
    thread_id = models.ForeignKey(CommentThread, on_delete=models.CASCADE)
    attachement= models.FileField('attachements/', null=True)
    creation_date = models.DateTimeField(default=timezone.now)


class Submission(models.Model):
    thread_id = models.ForeignKey(CommentThread, on_delete=models.CASCADE)
    task_id = models.OneToOneField(Task, on_delete=models.CASCADE)
    user_id = models.ForeignKey(UserDetail, on_delete=models.CASCADE)

