from .models import Assignment, AssignmentObject
from . import serializers
from . import filters

from django.http import Http404
from django.contrib.auth.models import User, Group

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import generics
import django_filters.rest_framework


class AssignmentDetail(APIView):
    
    def get_object(self, pk):
        try:
            return Assignment.objects.get(pk=pk)
        except Assignment.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        assign = self.get_object(pk)
        serializer = serializers.AssignmentSerializer(assign)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        assign = self.get_object(pk)
        serializer = serializers.AssignmentSerializer(assign, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        assign = self.get_object(pk)
        assign.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class AssignmentSet(generics.ListAPIView):
    serializer_class = serializers.AssignmentSerializer
    queryset = Assignment.objects.all()
    filter_backends = [django_filters.rest_framework.DjangoFilterBackend]
    filterset_class = filters.AssignmentFilter


class AssignmentNestedSet(generics.ListAPIView):
    serializer_class = serializers.NestedAssignmentSerializer
    queryset = Assignment.objects.all()
    filter_backends = [django_filters.rest_framework.DjangoFilterBackend]
    filterset_class = filters.AssignmentFilter



class AssignmentObjectSet(generics.ListAPIView):
    serializer_class = serializers.AssignmentObjectSerializer
    filter_backends = [django_filters.rest_framework.DjangoFilterBackend]

    def get_queryset(self):
        queryset = AssignmentObject.objects.all()

        actors = self.request.query_params.get('actors', None)
        if actors is not None:
            queryset = queryset.filter(taskObject__users=actors)
        
        assign_id = self.request.query_params.get('assign_id', None)
        if assign_id is not None:
            queryset = queryset.filter(assignment_id=assign_id)

        return queryset

"""
    User and Groups
"""
class ExtendedUserSet(generics.ListAPIView):
    serializer_class = serializers.UserSerializer
    queryset = User.objects.all()
    filter_backends = [django_filters.rest_framework.DjangoFilterBackend]
    filterset_class = filters.ExtendedUserFilter


class GroupList(generics.ListCreateAPIView):
    queryset = Group.objects.all()
    serializer_class = serailizers.GroupSerializer


class GroupDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Group.objects.all()
    serializer_class = serailizers.GroupSerializer