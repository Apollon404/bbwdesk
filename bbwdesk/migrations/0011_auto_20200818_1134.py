# Generated by Django 3.1 on 2020-08-18 11:34

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('bbwdesk', '0010_auto_20200817_1256'),
    ]

    operations = [
        migrations.AddField(
            model_name='assignment',
            name='actors',
            field=models.ManyToManyField(blank=True, to='bbwdesk.UserDetail'),
        ),
        migrations.AlterField(
            model_name='assignment',
            name='editor',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='assign_editor', to='bbwdesk.userdetail'),
        ),
    ]
