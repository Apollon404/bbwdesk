# Generated by Django 3.1 on 2020-08-20 12:09

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('bbwdesk', '0015_auto_20200820_1209'),
    ]

    operations = [
        migrations.AlterField(
            model_name='submission',
            name='task_id',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='bbwdesk.task'),
        ),
    ]
