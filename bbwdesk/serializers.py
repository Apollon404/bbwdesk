from . import models
from rest_framework import serializers
from django.contrib.auth.models import User

"""
------------- Task-Managment ----------------------------
"""

class TaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Task
        fields = ['id','title','description','users','priority','status']

class AssignmentObjectSerializer(serializers.ModelSerializer):
    taskObject = TaskSerializer(read_only=True)

    class Meta:
        model = models.AssignmentObject
        fields = ['id','assignment_id','taskObject']

class AssignmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Assignment
        fields = ['id','name','editor','actors']

class NestedAssignmentSerializer(AssignmentSerializer):
    assignObjects = AssignmentObjectSerializer(many=True, read_only=True)

    class Meta:
        model = models.Assignment
        fields = ['id','name','editor','actors', 'assignObjects']

"""
------------ User- Management -----------------
"""

class UserDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.UserDetail
        fields = ['initals']

class UserSerializer(serializers.ModelSerializer):
    detail = UserDetailSerializer(read_only=True)

    class Meta:
        model = User
        fields = ['id', 'username','first_name','last_name','detail']

class  ExtendedUserSerializer(serializers.Serializer):
    pass

class GroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = GroupSerializer
        fields = ['id','name']