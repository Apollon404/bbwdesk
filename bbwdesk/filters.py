from django_filters import rest_framework as filters
from . import models
from django.contrib.auth.models import User

class AssignmentFilter(filters.FilterSet):
    class Meta:
        model = models.Assignment
        fields = ['id','actors','name','editor']

class ExtendedUserFilter(filters.FilterSet):
    class Meta:
        model = User
        fields = ['id','username',]

class AssignmentObjectFilter(filters.FilterSet):
    class Meta:
        model = models.AssignmentObject
        fields = ['id']
