from .models import Task, Assignment, UserDetail, AssignmentObject


def objectToJson(object) -> dict:
    json = dict()
    initals = list()
    for user in object.users.all():
        initals.append(user.initals)

    json['type'] = str(object._meta)
    json['title'] = object.title
    json['actors'] = initals
    json['priority'] = object.priority
    json['status'] = object.status
    json['deadline'] = object.deadline
    json['pk'] = object.id

    return json


