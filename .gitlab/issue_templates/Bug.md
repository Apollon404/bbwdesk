Zusammenfassung

( Beschreibe hier den exakten Fehler)

Schritte zur Reproduktion

(Erkläre wie der Fehler wieder reproduziert werden kann)

Was ist das momentane Verhalten des Bugs?

(Was passiert ist)

Wie soll die Anwendung eigentlich arbeiten?

( Was man eigentlich sehen sollte)

Logdateien/ Screenshots
 
( Verlinke hier Screenshots, kopiere Ausschnitte aus Logadateien rein odet Konsolenasugabe und formatiere dies,
innerhalb  von Code-Blöcken (```))


Lösungsansätze

( Falls eine Vermutung besteht, hier den verantwortlichen Code des Projekts verlinken)

/label ~Bug ~Reproduziert ~Benötigt-Überprüfung
/cc @project-manager
/assing @qa-tester
